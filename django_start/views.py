from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
from django_start.db import execute_query


def get_sales(request):
    overall_sales = execute_query("""SELECT sum(UnitPrice * Quantity) FROM invoice_items;""")
    return HttpResponse(overall_sales)


def get_genres(request):
    duration_of_tracks = execute_query("""SELECT sum(Milliseconds/1000) FROM tracks
                                     GROUP BY GenreId;""")
    return HttpResponse(duration_of_tracks)


def get_greatest_hits(request):
    count = request.GET.get('count')
    if count:
        result = execute_query("""SELECT sum(UnitPrice * Quantity) as result
                              FROM invoice_items GROUP BY InvoiceId ORDER BY result 
                              DESC LIMIT ?;""", (count,))
    else:
        result = execute_query("""SELECT sum(UnitPrice * Quantity) as result
                                      FROM invoice_items GROUP BY InvoiceId ORDER BY result 
                                      DESC ;""")
    return HttpResponse(result)
